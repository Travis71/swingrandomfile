/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.IOException;
import java.util.List;
import pojo.Empleado;

/**
 *
 * @author Jadpa21
 */
public interface IDaoEmpleado extends IDao<Empleado>{
    Empleado findByCedula(String cedula) throws IOException;
    Empleado findByInss(String inss)throws IOException;
    List<Empleado> findByApellidos(String apellido)throws IOException;
    List<Empleado> findByRangoSalarial(double inf, double sup)throws IOException;
}
